from distutils.core import setup, Extension

rawterm = Extension('RawTerm',
                    define_macros = [('MAJOR_VERSION', '1'),
                                     ('MINOR_VERSION', '0')],
                    include_dirs = ['/usr/include/python3.6'],
                    libraries = [],
                    library_dirs = [],
                    sources = [ 'src/terminal.cpp',
                                'src/python_rawterm.cpp'])

editor = Extension('Editor',
                   define_macros = [('MAJOR_VERSION', '1'),
                                    ('MINOR_VERSION', '0')],
                   include_dirs = ['/usr/include/python3.6'],
                   libraries = [],
                   library_dirs = [],
                   sources = [ 'src/editor.cpp', 
                               'src/highlight.cpp',
                               'src/line.cpp',
                               'src/python_editor.cpp'])

setup (name = 'tx',
       version = '1.0',
       description = 'Short description here.',
       author = 'Jeremy Jurksztowicz',
       author_email = 'jurksztowicz@gmail.com',
       url = 'https://docs.python.org/extending/building',
       long_description = '''
A longer description and documentation strings go here!
''',
       ext_modules = [rawterm, editor])
