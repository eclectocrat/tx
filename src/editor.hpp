#ifndef _ec_editor_hpp
#define _ec_editor_hpp

#include "line.hpp"

namespace ec {

struct Terminal;

class Editor {
public:
    struct Vec2 { int x = 0; int y = 0; };
    using Line_iter = std::vector<Line>::iterator;
    
    // TODO: Change to set origin, size
    void set_term(Terminal&);

    void open_file(std::string const&);
    void save_file(std::string const&);

    void propagate_open_comment(Line_iter);
    
    std::string lines_to_string();
    
    void set_status_message(std::string const&);
    
    void move_right();
    void move_left();
    void move_down();
    void move_up();
    
    void insert_char(int);
    void insert_newline();
    void erase_char();
    
    void select_syntax_highlight(std::string const&, std::vector<Syntax> const& syntax_db);
    
    Vec2 calc_cursor();
    int xloc() const;
    int yloc() const;
    void move_cursor(int);
    void proc_key(int);
    void refresh_screen();
    
private:
    void update_line(Line_iter);
    void insert_line(Line_iter, std::string const&);

    Vec2 cursor;
    Vec2 offset;
    Vec2 screen_size;
    std::vector<Line> lines;
    const Line empty_line = {""};
    bool dirty = false;
    std::string filename;
    std::string statusmsg;
    time_t statusmsg_time;
    Syntax * syntax = nullptr;
    bool run = true;
};

}
#endif
