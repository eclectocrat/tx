/*
MIT License

Copyright (c) 2019 Jeremy Jurksztowicz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "highlight.hpp"

using namespace ec;

std::vector<std::string> C_file_exts = {".c", ".cpp", ".cc", ".cxx", ".h", ".hpp"};
std::vector<std::string> C_keywords = {
    "switch0", "if0",       "while0",  "for0",   "break0",   "continue0",
    "return0", "else0",     "struct0", "union0", "typedef0", "static0",
    "enum0",   "class0",    "int1",    "long1",  "double1",  "float1",
    "char1",   "unsigned1", "signed1", "void1"};

std::vector<Syntax> db{Syntax{C_file_exts, C_keywords, "//", "/*", "*/"}};
std::vector<Syntax> const& ec::syntax_db() { return db; }

