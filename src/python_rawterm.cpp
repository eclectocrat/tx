#define PY_SSIZE_T_CLEAN
#include <Python.h>
// ^ these need to stay before any system includes.
#include "terminal.hpp"

using namespace ec;

// Global terminal object
Terminal * terminal = nullptr;

extern "C" { 

// RawTerm.RawMode Python Object
struct Raw_mode {
    PyObject_HEAD
};

static PyObject * Raw_mode_enter(PyObject * self, PyObject *Py_UNUSED(ignored)) {
    assert(terminal);
    terminal->enable_raw_mode();
    return Py_None;
}

static PyObject * Raw_mode_exit(PyObject * self, PyObject *Py_UNUSED(ignored)) {
    assert(terminal);
    terminal->restore();
    return Py_None;
}

static PyObject * Raw_mode_read_key(PyObject * self, PyObject *Py_UNUSED(ignored)) {
    assert(terminal);
    return PyLong_FromLong(terminal->read_key());
}

// Export terminal codes _without_ any parameters.
#define EC_EXPORT(name) \
    static PyObject * Raw_mode_code_##name(PyObject * Py_UNUSED(self), PyObject *Py_UNUSED(ignored)) { return PyUnicode_FromString
#define EC_END ;}
EC_EXPORT(format_reset) 	(EC_TERM_FORMAT_RESET) 		EC_END
EC_EXPORT(format_bold)		(EC_TERM_FORMAT_BOLD) 		EC_END
EC_EXPORT(format_underline) 	(EC_TERM_FORMAT_UNDERLINE) 	EC_END
EC_EXPORT(format_inverse) 	(EC_TERM_FORMAT_INVERSE) 	EC_END
EC_EXPORT(cursor_home) 		(EC_TERM_CURSOR_HOME) 		EC_END
EC_EXPORT(cursor_save) 		(EC_TERM_CURSOR_SAVE) 		EC_END
EC_EXPORT(cursor_restore) 	(EC_TERM_CURSOR_RESTORE) 	EC_END
EC_EXPORT(cursor_hide) 		(EC_TERM_CURSOR_HIDE) 		EC_END
EC_EXPORT(cursor_show) 		(EC_TERM_CURSOR_SHOW)	 	EC_END
EC_EXPORT(clear_line_left) 	(EC_TERM_CLEAR_LINE_LEFT) 	EC_END
EC_EXPORT(clear_line_right) 	(EC_TERM_CLEAR_LINE_RIGHT) 	EC_END
EC_EXPORT(clear_line_all) 	(EC_TERM_CLEAR_LINE_ALL) 	EC_END
#undef EC_EXPORT
#undef EC_END

static PyObject * Raw_mode_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    assert(terminal);
    static char * kwlist[] = {"defer_entry"};
    int defer_entry = 0;
    if (not PyArg_ParseTupleAndKeywords(args, kwds, "|p", kwlist, &defer_entry)) {
    	return NULL;
    }
    if (not defer_entry) {
    	terminal->enable_raw_mode();
    }
    return reinterpret_cast<PyObject*>(type->tp_alloc(type, 0));
}

static void Raw_mode_dealloc(Raw_mode * self) {
    Raw_mode_exit(NULL, NULL);
}

static PyMethodDef Raw_mode_methods[] = {
    {"enter_raw_mode", 		Raw_mode_enter,			METH_VARARGS, 	"Sets the terminal to raw IO mode."},
    {"restore_previous_mode", 	Raw_mode_exit, 			METH_VARARGS, 	"Restores the terminal to the termios settings before this mode was entered."},
    {"read_key", 		Raw_mode_read_key, 		METH_VARARGS, 	"Blocks until an input key is available."},
    {"code_format_reset", 	Raw_mode_code_format_reset, 	METH_VARARGS, 	""},
    {"code_format_bold", 	Raw_mode_code_format_bold, 	METH_VARARGS, 	""},
    {"code_format_underline", 	Raw_mode_code_format_underline, METH_VARARGS, 	""},
    {"code_format_inverse", 	Raw_mode_code_format_inverse, 	METH_VARARGS, 	""},
    {"code_cursor_home", 	Raw_mode_code_cursor_home, 	METH_VARARGS, 	""},
    {"code_cursor_save", 	Raw_mode_code_cursor_save, 	METH_VARARGS, 	""},
    {"code_cursor_restore", 	Raw_mode_code_cursor_restore, 	METH_VARARGS, 	""},
    {"code_cursor_hide", 	Raw_mode_code_cursor_hide, 	METH_VARARGS, 	""},
    {"code_cursor_show", 	Raw_mode_code_cursor_show, 	METH_VARARGS, 	""},
    {"code_line_clear_left", 	Raw_mode_clear_line_left, 	METH_VARARGS, 	""},
    {"code_line_clear_right", 	Raw_mode_clear_line_right, 	METH_VARARGS, 	""},
    {"code_line_clear_all", 	Raw_mode_clear_line_all, 	METH_VARARGS, 	""},
    {NULL, NULL, 0, NULL}
};

static PyTypeObject Raw_mode_type = {
    PyVarObject_HEAD_INIT(NULL, 0)
};

void build_Raw_mode_type() {
    Raw_mode_type.tp_name 	= "terminal.RawMode";
    Raw_mode_type.tp_basicsize 	= sizeof(Raw_mode);
    Raw_mode_type.tp_dealloc 	= (destructor)Raw_mode_dealloc;
    Raw_mode_type.tp_flags 	= Py_TPFLAGS_DEFAULT;
    Raw_mode_type.tp_methods 	= Raw_mode_methods;
    Raw_mode_type.tp_new 	= Raw_mode_new;
}

// RawTerm Python Module
static PyModuleDef module = {
    PyModuleDef_HEAD_INIT
};

void build_module() {
    module.m_name = "RawTerm";
    module.m_doc  = "docstring goes here.";
    module.m_size = -1;
}

void dealloc_terminal() {
    delete terminal;
}

PyMODINIT_FUNC PyInit_terminal() {
    if (PyType_Ready(&Raw_mode_type) < 0) {
    	return NULL;
    }
    build_module();
    PyObject * py_module = PyModule_Create(&module);
    if (py_module == NULL) {
    	return NULL;
    }
    Py_INCREF(&Raw_mode_type);
    if (PyModule_AddObject(py_module, "RawMode", (PyObject*)&Raw_mode_type) < 0) {
    	Py_DECREF(&Raw_mode_type);
	Py_DECREF(&module);
	return NULL;
    }

    { using namespace Keycode;
    #define EC_EXPORT(key) \
    	PyModule_AddIntConstant((PyObject*)&module, #key, key);
    	EC_EXPORT(ESC)
    	EC_EXPORT(ENTER)
    	EC_EXPORT(BACKSPACE)
    #undef EC_EXPORT
    }

    terminal = new Terminal;
    if (Py_AtExit(dealloc_terminal) != 0) {
   	fprintf(stderr, "WARNING: Failed to register cleanup function for Terminal.");
    }
    return py_module;
}

} // extern "C"
